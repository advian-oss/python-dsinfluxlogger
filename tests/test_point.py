"""Test point creation related stuff"""
import uuid
import logging
import collections.abc

from datastreamcorelib.datamessage import PubSubDataMessage

from dsinfluxlogger.service import InfluxLoggerService


LOGGER = logging.getLogger(__name__)


def test_flatten_array() -> None:
    """Test that array values are flattened correctly"""
    msg = PubSubDataMessage(
        topic=b"orientation_quaternion",
        messageid=uuid.UUID("f8fbbfae-5bf8-4b05-8668-9027a81be683"),
        data={
            "systemtime": "2021-09-14T07:13:45.103534Z",
            "source": "gnssservice.plugins.anpp",
            "talker": "ANPP",
            "qs": -0.041475169360637665,
            "qx": -0.26773136854171753,
            "qy": 0.9613495469093323,
            "qz": -0.04905951768159866,
            "stddev": [0.007920963689684868, 0.06550817936658859, 0.018649285659193993, 0.007769450545310974],
            "topic": "orientation_quaternion",
            "msgtype": "gnssservice.messages.QuaternionOrientation",
        },
    )
    point = InfluxLoggerService.message_to_point({"measurement": "test"}, msg)
    for fieldname in point["fields"]:
        value = point["fields"][fieldname]
        LOGGER.debug("key={} value={}".format(fieldname, repr(value)))
        assert not isinstance(value, (list, tuple, collections.abc.Mapping))
    assert point["fields"]["stddev_2"] == 0.018649285659193993
